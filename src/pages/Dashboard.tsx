import * as React from "react";
import { Link } from "react-router-dom";
import { graphqlOperation, API } from "aws-amplify";
import { onCreateGame } from "../graphql/subscriptions";
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip } from 'recharts';

interface ChartProps {
    data: Array<{
        black: number,
        orange: number,
        blue: number,
        second: number
    }>
}

interface LabelProps {
    type: string;
    count: any;
}

interface Game {
    id: string,
    timestamp: string,
    type: string
}

const initalChart = [
    {
        black: 0,
        orange: 0,
        blue: 0,
        second: 0
    },
    {
        black: 0,
        orange: 0,
        blue: 0,
        second: 1
    },
    {
        black: 0,
        orange: 0,
        blue: 0,
        second: 2
    },
    {
        black: 0,
        orange: 0,
        blue: 0,
        second: 3
    },
    {
        black: 0,
        orange: 0,
        blue: 0,
        second: 4
    },
    {
        black: 0,
        orange: 0,
        blue: 0,
        second: 5
    }
];

function RenderChart(props: ChartProps) {
    return (
        <div id="app-dashboard-chart">
            <LineChart width={800} height={400} data={props.data} margin={{ top: 5, right: 20, bottom: 5, left: 0 }}>
                <Line type="monotone" dot={false} strokeWidth={2} dataKey="orange" stroke="orange" width={30} />
                <Line type="monotone" dot={false} strokeWidth={2} dataKey="blue" stroke="blue" />
                <Line type="monotone" dot={false} strokeWidth={4} dataKey="black" stroke="black" />
                <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
                <XAxis label={{ value: "Second(s)", position: "insideBottom" }} height={40} dataKey="second" />
                <YAxis label={{ value: "Click(s)", position: "insideLeft", transform: "rotate(-90, 25, 185)" }} />
                <Tooltip />
            </LineChart>
        </div>
    )
}

export class Dashboard extends React.Component {
    updateItemListener: any;
    state = {
        chart: initalChart,
        data: [],
        startTime: Date.now(),
        duration: 5000,
        increment: 500,
        blueCnt: 0,
        orangeCnt: 0
    }
    componentDidMount() {
        var updateItemListener = API.graphql(
            graphqlOperation(onCreateGame)
        ).subscribe({
            next: (game: any) => {
                var data: Array<Game> = this.state.data;
                var blue = this.state.blueCnt;
                var orange = this.state.orangeCnt;
                if (game.value.data.onCreateGame.type === "blue") blue++;
                if (game.value.data.onCreateGame.type === "orange") orange++;
                data.push(game.value.data.onCreateGame);
                this.setState({ data: data, blueCnt: blue, orangeCnt: orange })
            }
        });
        setTimeout(() => {
            updateItemListener.unsubscribe();
            this.setChartData();
        }, 5000)
    }

    setChartData = () => {
        var startTime = this.state.startTime;
        var chart = [];
        for (var time = 0; time <= this.state.duration; time = time + this.state.increment) {
            var obj = {
                second: time / 1000,
                black: 0,
                orange: 0,
                blue: 0
            }
            this.state.data.map((game: Game) => {
                var diff = new Date(game.timestamp).getTime() - startTime;
                if (time < diff && diff < time + this.state.increment) {
                    switch (game.type) {
                        case "blue":
                            obj.blue++;
                            break;
                        case "orange":
                            obj.orange++
                    }
                }
            })
            obj.black = obj.blue - obj.orange
            chart.push(obj);
        }
        this.setState({ chart: chart });
    }

    render() {
        return (
            <div id="app-dashboard">
                <RenderChart data={this.state.chart}></RenderChart>
                <div className="app-row">
                    <label className="dashboard-label" style={{ background: 'orange' }}>{this.state.orangeCnt}</label>
                    <label className="dashboard-label" style={{ background: 'blue' }}>{this.state.blueCnt}</label>
                </div>
                <p>Please go to <Link to="/client" target="_blank"> Client Page </Link> to join the game.</p>
            </div>
        );
    }
}