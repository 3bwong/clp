import * as React from "react";
import { Connect } from "aws-amplify-react";
import { graphqlOperation } from "aws-amplify";
import { createGame } from "../graphql/mutations";

interface Props {
    type: string;
}


function CreateGameButton(props: Props) {
    return (
        <Connect mutation={graphqlOperation(createGame)}>
            {({ mutation }: any) => (
                <button
                    className="button round"
                    style={{ background: props.type }}
                    type="button"
                    onClick={() => {
                        mutation({
                            input: {
                                type: props.type,
                                timestamp: new Date().toISOString()
                            }
                        });
                    }}>
                    {(props.type === 'blue') ? '+' : '-'}
                </button>
            )}
        </Connect>
    )
}

export class Client extends React.Component {
    render() {
        return (
            <div id="app-client" className="app-row">
                <CreateGameButton type="orange"></CreateGameButton>
                <CreateGameButton type="blue"></CreateGameButton>
            </div>
        );
    }
}