import * as React from "react";
import { Link } from "react-router-dom";

export class Home extends React.Component {
    render() {
        return (
            <div id="app-home">
                <div id="app-home-content">
                    <h1> CLP Assessment</h1>
                    <Link className="app-home-nav" to="/dashboard" target="_blank"> Dashboard </Link>
                    <Link className="app-home-nav" to="/client" target="_blank"> Client </Link>
                </div>
            </div>
        );
    }
}