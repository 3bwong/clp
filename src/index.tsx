import { BrowserRouter as Router } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';
import Amplify from '@aws-amplify/core'
import config from './aws-exports';
import ReactDOM from 'react-dom';
import React from 'react';
import App from './App';
import './index.css';

Amplify.configure(config)

ReactDOM.render(<Router><App /></Router> , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
