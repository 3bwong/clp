import { Route, Switch } from 'react-router-dom';
import { Dashboard } from './pages/Dashboard';
import { Client } from './pages/Client';
import { Home } from './pages/Home';
import React from 'react';
import './App.css';

const App: React.FC = () => {
  return (
    <Switch>
      <Route component={Home} path="/" exact />
      <Route path="/client" component={Client} exact />
      <Route path="/dashboard" component={Dashboard} exact />
    </Switch>
  );
}

export default App;
