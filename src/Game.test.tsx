import API, { graphqlOperation } from "@aws-amplify/api";
import { createGame } from "./graphql/mutations";
import { listGames } from "./graphql/queries";
import config from "./aws-exports.js";
API.configure(config);

describe('Amplify Operation', () => {
    test('create', async () => {
        var blue = await API.graphql(graphqlOperation(createGame, { input: { timestamp: new Date().toISOString(), type: 'blue' } }));
        console.log('Created:' + JSON.stringify(blue.data.createGame))
        expect(blue).toBeTruthy();
        var orange = await API.graphql(graphqlOperation(createGame, { input: { timestamp: new Date().toISOString(), type: 'orange' } }));
        console.log('Created:' + JSON.stringify(orange.data.createGame))
        expect(orange).toBeTruthy();
    })
    test('listing', async () => {
        var games = await API.graphql(graphqlOperation(listGames));
        console.log('Current List Length: ' + games.data.listGames.items.length)
        expect(games.data.listGames.items.length).toBeGreaterThan(0);
    })
})