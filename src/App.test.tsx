import { render, fireEvent } from '@testing-library/react';
import { MemoryRouter } from 'react-router';
import config from "./aws-exports.js";
import API from "@aws-amplify/api";
import React from 'react';
import App from './App';
API.configure(config);

describe("Location Validation", () => {
  it('valid path to Home (/)', () => {
    const { container } = render(
      <MemoryRouter initialEntries={['/']}>
        <App />
      </MemoryRouter>
    );
    expect(container.querySelector("#app-home")).toBeTruthy();
  });

  it('valid path to Dashboard (/dashboard)', () => {
    const { container } = render(
      <MemoryRouter initialEntries={['/dashboard']}>
        <App />
      </MemoryRouter>
    );
    expect(container.querySelector("#app-dashboard")).toBeTruthy();
  });

  it('valid path to Client (/client)', async() => {
    const { container } = render(
      <MemoryRouter initialEntries={['/client']}>
        <App />
      </MemoryRouter>
    );
    expect(container.querySelector("#app-client")).toBeTruthy();
  });
})